# Vilmate Test Task

- [Install](#markdown-header-install)
    - [Global packages](#markdown-header-global-packages)
    - [Local packages](#markdown-header-local-packages)
- [Seed and clean database](#markdown-header-seed-and-clean-database)
- [Usage](#markdown-header-usage)
    - [Run API Server](#markdown-header-run-api-server)
- [TODO](#markdown-header-todo)
  

## Install
### Global packages

- Node 8.4.0
- MongoDB was installed with docker: 
```bash
    docker run --name vilmate-test-mongo -p 27017:27017 -d mongo
    docker start vilmate-test-mongo
```

### Local packages
`npm install`

## Seed and clean database

Run `npm run db:seed` in order to seed DB with test data.
Run `npm run db:clean` in order to clean DB.

## Usage

### Run API Server
Run `npm start`. API is available on [http://localhost:8081](http://localhost:8081).

### Try it
First of all you should use Basic Authorization. So provide `Authorization` header for each request. You can find auth data in `./config.json` file - see `http.auth` section.

You can try it with [Postman](https://www.getpostman.com/).

An already made Postman requests collection based on documentation can be found in `./postman/vilmate-test.postman_collection.json` file. Simply import it in Postman app and try any requests you want.

If you don't prefer Postman simply use this API to test the requests:

1. GET [http://localhost:8081/messages](http://localhost:8081/messages) - list all messages. You can specify `page={number}` for pagination.

2. GET [http://localhost:8081/messages/archived](http://localhost:8081/messages/archived) - list all archived messages. You can specify `page={number}` for pagination.

3. GET `http://localhost:8081/messages/:uid` -  show message by UID.

4. PATCH `http://localhost:8081/messages/:uid` with body: 
 ` { "action": "READ" }` - read message by UID.

5. PATCH `http://localhost:8081/messages/:uid` with body: 
    ` { "action": "ARCHIVE" }` - read message by UID.

## TODO
1. Cover API with unit & functional tests using test DB with mock data.
2. Add conditions for filtering the list of all/archived messages: for example, find only read or not yet read ones.
3. Add TSLint.
4. Cover the app with unit tests.
