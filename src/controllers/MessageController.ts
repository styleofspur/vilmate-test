import { IMessage }     from '../db/schemas/Message';
import { MessageModel } from '../db/models';

enum MessageControllerActions {
  READ    = 'READ',
  ARCHIVE = 'ARCHIVE'
}

export default class MessageController {

  /**
   * @public
   * @static
   * @readonly
   * @type {number}
   */
  // @todo: include this param in the API requirements doc
  public static readonly DEFAULT_ITEMS_LIMIT: number = 10;

  /**
   *
   * @public
   * @static
   * @param req
   * @param res
   * @param {Function} next
   * @returns {Promise<void>}
   */
  public static async getAll(req, res, next) {
    try {
      const params = req.fetchParameter([
        'number:page'
      ]);

      const messages: IMessage[] = await MessageModel
        .find()
        .skip(MessageController.DEFAULT_ITEMS_LIMIT * (params.page > 1 ? params.page : 0))
        .limit(MessageController.DEFAULT_ITEMS_LIMIT);

      res.status(200);
      res.json(messages);
    } catch (err) {
      next(err);
    }

  }

  /**
   *
   * @public
   * @static
   * @param req
   * @param res
   * @param {Function} next
   * @returns {Promise<void>}
   */
  public static async getArchived(req, res, next) {
    try {
      const params = req.fetchParameter([
        'number:page'
      ]);

      const messages: IMessage[] = await MessageModel
        .find({
          archived: true
        })
        .skip(MessageController.DEFAULT_ITEMS_LIMIT * (params.page > 1 ? params.page : 0))
        .limit(MessageController.DEFAULT_ITEMS_LIMIT);

      res.status(200);
      res.json(messages);
    } catch (err) {
      next(err);
    }

  }

  /**
   *
   * @public
   * @static
   * @param req
   * @param res
   * @param {Function} next
   * @returns {Promise<void>}
   */
  public static async getOne(req, res, next): Promise<void> {
    try {
      const message: IMessage = await MessageModel.findOne({
        uid: req.params.uid
      });

      if (message) {
        res.status(200);
        res.json(message);
      } else {
        next({
          status: 404,
          message: `Message with #UID ${req.params.id} not found`
        });
      }

    } catch (err) {
      next(err);
    }

  }

  /**
   *
   * @public
   * @static
   * @param req
   * @param res
   * @param {Function} next
   * @returns {Promise<void>}
   */
  public static async doAction(req, res, next): Promise<void> {
    try {
      const message = await MessageModel.findOne({
        uid: req.params.uid
      });

      if (!message) {
        next({
          status: 404,
          message: `Message with #UID ${req.params.id} does not exist`
        });
      }

      switch (req.body.action) {
        case MessageControllerActions.ARCHIVE: {
          return MessageController._archive(message, { res, next });
        }
        case MessageControllerActions.READ: {
          return MessageController._read(message, { res, next });
        }
        default: {
          next({
            status:  400,
            message: `Unknown action ${req.body.action}. Supported actions are: ${Object.keys(MessageControllerActions)}`
          });
        }
      }

    } catch (err) {
      next(err);
    }

  }

  /**
   *
   * @private
   * @static
   * @param {IMessage} message
   * @param {res, next}
   * @returns {Promise<void>}
   */
  private static async _archive(message: IMessage, { res, next }): Promise<void> {
    try {
      message.archived = !message.archived;
      await message.save();

      res.status(204);
      res.end();
    } catch (err) {
      next(err);
    }
  }

  /**
   *
   * @private
   * @static
   * @param {IMessage} message
   * @param {res, next}
   * @returns {Promise<void>}
   */
  private static async _read(message: IMessage, { res, next }): Promise<void> {
    try {
      message.read = !message.read;
      await message.save();

      res.status(204);
      res.end();
    } catch (err) {
      next(err);
    }
  }

}