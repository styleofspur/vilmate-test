import * as express          from 'express';
import * as authorization    from 'auth-header';
import * as reqParamsFetcher from 'express-param';
import * as http             from 'http';
import * as bodyParser       from 'body-parser';
import * as cors             from 'cors';

// configuration & helpers
import config      from './config';
import MongoHelper from './helpers/mongo';

// controllers
import MessageController from './controllers/MessageController';

const app = express();
app.use(reqParamsFetcher());
console.log('App starting...');

MongoHelper.init().then(() =>{
    console.log('Database initialized');

    // Create HTTP server
    const server = http.createServer(app);
    const port   = config.get('http:port');

    app.use(cors());
    app.use(bodyParser.json());

    // routing

    // list messages
    app.get('/messages', authorize, MessageController.getAll);
    // list archived messages
    app.get('/messages/archived', authorize, MessageController.getArchived);
    // show message
    app.get('/messages/:uid', authorize, MessageController.getOne);
    /*
     Partially update message.
     Usage of PATCH method is based on: https://stackoverflow.com/a/24241955
     */
    app.patch('/messages/:uid', authorize, MessageController.doAction);

    // error handling
    app.use((err, req, res, next) => {
        console.error(err.stack || err);
        res.status(err.status || 500);
        res.json({
            message: err.message || 'Error occurred'
        });
    });

    // Start the HTTP server
    server.listen(port, () => {
        console.log('HTTP Server started at port ' + server.address().port);
    });
}).catch(console.error);

/**
 * Does Basic Authorization.
 *
 * @param req
 * @param res
 * @param next
 * @returns {void}
 */
function authorize(req, res, next): void {

    // Something messed up.
    const fail = () => {
        res.set('WWW-Authenticate', authorization.format('Basic'));
        res.status(401).send();
    };

    // Get authorization header.
    if (!req.get('authorization')) {
        return fail();
    }
    const auth = authorization.parse(req.get('authorization'));

    // No basic authentication provided.
    if (auth.scheme !== 'Basic') {
        return fail();
    }

    // Get the basic auth component.
    const [un, pw] = new Buffer(auth.token, 'base64').toString().split(':', 2);

    // Verify authentication.
    if (
        un === config.get('http:auth:un') &&
        pw === config.get('http:auth:pw')) {
        next();
    } else {
      return fail();
    }
}
