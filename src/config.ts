import * as nconf from 'nconf';
import * as path  from 'path';

nconf
  .argv()
  .env()
  .file('global', {
    file: path.resolve(__dirname, '..', 'config.json')
  });

export default nconf;