import MessageSchema from './schemas/Message';

export let MessageModel;

export const init = connection => {
    MessageModel = connection.model('message', MessageSchema);
};
