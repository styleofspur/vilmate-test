import { Document as IMongooseItem } from 'mongoose';

export default {
  uid: {
    type:     String,
    required: true,
    unique:   true
  },
  sender: {
    type:     String,
    required: true
  },
  subject: {
    type:     String,
    required: true
  },
  message: {
    type:     String,
    required: true
  },
  time_sent: {
    type:    Date,
    default: Date.now
  },
  read: {
    type:    Boolean,
    default: false
  },
  archived: {
    type:    Boolean,
    default: false
  }
};

export interface IMessage extends IMongooseItem {
  uid:       string;
  sender:    string;
  subject:   string;
  message:   string;
  time_sent: Date;
  read?:     boolean;
  archived?: boolean
}
