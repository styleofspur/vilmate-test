import * as fs   from 'await-fs';
import { argv }  from 'yargs';
import * as path from 'path';

import MongoHelper      from '../helpers/mongo';
import { MessageModel } from './models';

const DB_DEFAULT_FILE_PATH = path.resolve(__dirname, '..', '..', 'data/messages_sample.json');
const dbFilePath           = argv[0] || DB_DEFAULT_FILE_PATH;

MongoHelper.init()
    .then(async () => {
      console.log('Database initialized');

      const data = JSON.parse(await fs.readFile(dbFilePath));

      if (!data || !data.messages || !Array.isArray(data.messages)) {
          throw new Error('Invalid DB data: expecting JSON with object with "messages" array property');
      }

      const promises = [];
      data.messages.forEach(message => {
          message.read = message.archived = false;
          promises.push((new MessageModel(message)).save())
      });

      return Promise.all(promises);
    })
    .then(() => {
        console.log('DB seed is done');
        process.exit(0);
    })
    .catch((err: Error) => {
        console.error(err);
        process.exit(1);
    });
