import MongoHelper      from '../helpers/mongo';
import { MessageModel } from './models';

MongoHelper.init()
    .then(() => {
        console.log('Database initialized');
        return MessageModel.remove();
    })
    .then(() => {
        console.log('DB clean is done');
        process.exit(0);
    })
    .catch((err: Error) => {
      console.error(err);
      process.exit(1);
    });